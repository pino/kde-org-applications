<?php

/**
 * SPDX-FileCopyrightText: 2020 Carl Schwan <carl@carlschwan.eu>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

namespace App\Twig;

use Twig\Error\Error;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class AppTransExtension extends AbstractExtension
{
    public function getFilters()
    {
        return [
            new TwigFilter('l10n', [$this, 'l10n'])
        ];
    }

    /**
     * @param array $array
     * @param string $locale
     * @return string
     * @throws Error
     */
    public function l10n(array $array, string $locale): string
    {
        if (!is_array($array)) {
            // No translations, nothing to do
            throw new Error("Error in l10n filter: not an array");
        }

        if (sizeof($array) === 0) {
            //print "Empty l10n array";
            return "";
        }

        if ($locale === 'en') {
            $locale = 'C';
        }

        if (array_key_exists($locale, $array)) {
            return $array[$locale];
        }
        return $array['C'];
    }
}
