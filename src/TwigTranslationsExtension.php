<?php
/**
 * SPDX-FileCopyrightText: 2020 Carl Schwan <carl@carlschwan.eu>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

namespace App;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class TwigTranslationsExtension extends AbstractExtension
{
    public function getFunctions()
    {
        return [
            new TwigFunction('_', '\gettext'),
        ];
    }}
