<?php
/**
 * SPDX-FileCopyrightText: 2020 David Barchiesi <david@barchie.si>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

namespace App\Model;

/**
 * Enum ReleaseType
 * @package App\Model
 */
abstract class ReleaseType {
    const Stable = 0;
    const Development = 1;
}
