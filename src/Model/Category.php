<?php

/**
 * SPDX-FileCopyrightText: 2020 Carl Schwan <carl@carlschwan.eu>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
namespace App\Model;

/**
 * Class Category
 * @package App\Model
 */
class Category
{
    /** @var string */
    private $name;

    /** @var array */
    private $applications;

    /** @var array */
    private $addons;

    private $applicationNames;

    static private $categoryIcons = [
        "Development" => "development.svg",
        "Education" => "education.svg",
        "Games" => "games.svg",
        "Graphics" => "graphics.svg",
        "Internet" => "internet.svg",
        "Multimedia" => "multimedia.svg",
        "Office" => "office.svg",
        "Settings" => "settings.svg",
        "System" => "system.svg",
        "Utilities" => "utilities.svg",
    ];

    public function __construct(string $name, array $applicationNames)
    {
        $this->name = $name;
        $this->applications = [];
        $this->addons = [];

        foreach ($applicationNames as $applicationName) {
            $application = AppData::fromName($applicationName);
            if ($application->getType() === AppDataType::Application
                || $application->getType() === AppDataType::Console) {
                $this->applications[] = $application;
            } else {
                $this->addons[] = $application;
            }
        }
    }

    public function getApplications()
    {
        return $this->applications;
    }

    public function getAddons()
    {
        return $this->addons;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string|null
     */
    public function getIcon(): ?string
    {
        if (array_key_exists($this->name, self::$categoryIcons)) {
            return self::$categoryIcons[$this->name];
        }
        return null;
    }
}
