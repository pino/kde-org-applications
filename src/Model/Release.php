<?php
/**
 * SPDX-FileCopyrightText: 2020 David Barchiesi <david@barchie.si>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

namespace App\Model;

use App\Model\ReleaseType;

class Release
{
    private $version = null;
    /** @var ReleaseType */
    private $type;
    private $timestamp = null;

    public function __construct(string $version, int $type, string $timestamp)
    {
        $this->version = $version;
        $this->type = $type;
        $this->timestamp = $timestamp;
    }

    public static function fromData(array $release): ?Release
    {
        if ($release['type'] === 'stable') {
            return new Release($release['version'], ReleaseType::Stable, $release['unix-timestamp']);
        } else if ($release['type'] === 'development') {
            return new Release($release['version'], ReleaseType::Development, $release['unix-timestamp']);
        } else  {
            return null;
        }
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @return string|null
     */
    public function getVersion(): ?string
    {
        return $this->version;
    }

    /**
     * @return int|null
     */
    public function getTimestamp(): ?int
    {
        return $this->timestamp;
    }

}
