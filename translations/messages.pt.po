msgid ""
msgstr ""
"Project-Id-Version: kde-org-applications\n"
"PO-Revision-Date: 2020-06-18 10:31+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-POFile-SpellExtra: name es Discover AppStream Obtê Droid Play\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

msgid "KDE's Applications"
msgstr "Aplicações do KDE"

msgid ""
"KDE is a community of friendly people who create over 200 apps which run on "
"any Linux desktop, and often other platforms too. Here is the complete list."
msgstr ""
"O KDE é uma comunidade de pessoas amigáveis que criam mais de 200 aplicações "
"que correm em qualquer ambiente de trabalho Linux, assim como noutras "
"plataformas. Aqui está a lista completa."

msgid "Search..."
msgstr "Procurar..."

msgid "%name% Icon"
msgstr "Ícone do %name%"

msgid "Get Involved"
msgstr "Envolva-se"

msgid "Install on Linux"
msgstr "Instalar no Linux"

msgid "This app is unmaintained and no longer released by the KDE community."
msgstr ""
"Esta aplicação não está a ser mantida e não é mais providenciada pela "
"comunidade do KDE."

msgid "Screeenshot of %name%"
msgstr "Imagem do %name%"

msgid "Details for %name%"
msgstr "Detalhes do %name%"

msgid "Developer website"
msgstr "Página Web do programador"

msgid "Latest stable release"
msgstr "Última versão estável"

msgid "Latest development release"
msgstr "Última versão de desenvolvimento"

msgid "License"
msgstr "Licença"

msgid "Get Help"
msgstr "Obter Ajuda"

msgid "%name% Handbook"
msgstr "Manual do %name%"

msgid "KDE Community Forum"
msgstr "Fórum da Comunidade KDE"

msgid "Contact the authors"
msgstr "Contactar os autores"

msgid "Report a bug"
msgstr "Comunicar um erro"

msgid "IRC:"
msgstr "IRC:"

msgid "Mailing List:"
msgstr "Lista de Correio:"

msgid "Author(s):"
msgstr "Autor(es):"

msgid "Thanks to:"
msgstr "Agradecimentos a:"

msgid ""
"This button only works with <a\n"
"href=\"/applications/system/org.kde.discover\">Discover</a> and other\n"
"AppStream application stores. You can also use your distribution's package "
"manager."
msgstr ""
"Este botão só funciona com o <a\n"
"href=\"/applications/system/org.kde.discover\">Discover</a> e com\n"
"outras lojas de aplicações AppStream. Também poderá usar o gestor de pacotes "
"da sua distribuição."

msgid "Get it from Microsoft"
msgstr "Obtê-lo da Microsoft"

msgid "Install on Windows"
msgstr "Instalar no Windows"

msgid "Get it from F-Droid"
msgstr "Obtê-lo do F-Droid"

msgid "Get it on F-Droid"
msgstr "Obtê-lo no F-Droid"

msgid "Get it on Google Play"
msgstr "Obtê-lo no Google Play"

msgid "Products"
msgstr "Produtos"

msgid "Develop"
msgstr "Desenvolvimento"

msgid "Donate"
msgstr "Doar"

msgid "Unmaintained"
msgstr "Não Mantido"

msgid "Applications"
msgstr "Aplicações"

msgid "Addons"
msgstr "Extensões"
